package app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.entity.User;
import app.repository.UserRepository;

@Component
public class UserDAO {

	@Autowired
	UserRepository userRepository;
	
	
	public void registerUser(User user) {
		userRepository.save(user);
	}
	
}
