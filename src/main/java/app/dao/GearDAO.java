package app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import app.entity.Gear;
import app.repository.GearRepository;

@Component
public class GearDAO {

	
		@Autowired
		GearRepository gearRepository;
	
		public List<Gear> getAllGears(){
			List<Gear> gears = Lists.newArrayList(gearRepository.findAll());
			return gears;
		}
		
		public Optional<Gear> getGear(int id) {
			return gearRepository.findById(id);
		}
		
		public Gear addGear(Gear gear) {
			System.out.println(gear);
			return gearRepository.save(gear);
		}
		
}
