package app.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.dao.GearDAO;
import app.dto.GearDTO;
import app.entity.Gear;

@Component
public class GearService {

	
	@Autowired
	GearDAO gearDAO;
	
	public List<Gear> getAllGears() {
		return gearDAO.getAllGears();
	}
	
	public Optional<Gear> getGears(int id) {
		return gearDAO.getGear(id);
	}
	

	public Gear addGear(Gear gear) {
		 return gearDAO.addGear(gear);
	}
	
	public Gear updateGear(Gear gear) {		
		return gearDAO.addGear(gear);
	}
}
