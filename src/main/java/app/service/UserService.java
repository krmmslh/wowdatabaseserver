package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.dao.UserDAO;
import app.entity.User;

@Component
public class UserService {

	@Autowired
	private UserDAO userDAO;
	
	
	public void registerUser(User user) {
		userDAO.registerUser(user);
	}
	
	
}
