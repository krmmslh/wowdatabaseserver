package app;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.dto.GearDTO;
import app.entity.Gear;
import app.service.GearService;

@RestController
public class GearController {

	@Autowired
	private GearService gearService;
	
	
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
   
    @GetMapping(value = "/gears" )
    public ResponseEntity getGears() {
    	return new ResponseEntity(entitiestoDTO(gearService.getAllGears()), HttpStatus.OK);
    }

    @GetMapping(value = "/gear/{id}" )
    public ResponseEntity getGears(@PathVariable(value="id") String id) {
    	int parsedId = -1;
    	try {
    		parsedId = Integer.parseInt(id);
    	} catch (NumberFormatException c) {
    		System.out.println("RATE");
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not a number");
    	}
    	Optional<Gear> g = gearService.getGears(parsedId);
    	if (g.isPresent()) {
        	return ResponseEntity.status(HttpStatus.OK).body(entitiytoDTO(g.get()));
 
    	}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No Gear found for this ID");
    }
    
    @PostMapping(value = "/gear" )
    public ResponseEntity createGear(@RequestBody GearDTO gearDTO) {
    	if (gearDTO.getAttr() == null || gearDTO.getName() == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(-1);
    	}
    	Gear gear = new Gear(gearDTO.getName(), gearDTO.getAttr());
    	return  ResponseEntity.status(HttpStatus.OK).body(entitiytoDTO(gearService.addGear(gear)));
    }
    
    @PutMapping(value = "/gear/{id}" )
    public ResponseEntity updateGear(@RequestBody GearDTO gearDTO) {
    	if (gearDTO.getId() < 0 || gearDTO.getAttr() == null || gearDTO.getName() == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(-1);
    	}
    	Gear gear = new Gear(gearDTO.getId(), gearDTO.getName(), gearDTO.getAttr());
    	return  ResponseEntity.status(HttpStatus.OK).body(entitiytoDTO(gearService.updateGear(gear)));
    }
    
    public List<GearDTO> entitiestoDTO(List<Gear> gears) {
    	return gears.stream().map(g -> new GearDTO(g.getId(), g.getName(), g.getAttr())).collect(Collectors.toList());
    }
    
    public GearDTO entitiytoDTO(Gear gear) {
    	return new GearDTO(gear.getId(), gear.getName(), gear.getAttr());
    }
    

}