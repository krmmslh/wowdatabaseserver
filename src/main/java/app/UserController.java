package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.dto.UserDTO;
import app.entity.User;
import app.service.UserService;


@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
    @PostMapping(value = "/user" )
    public ResponseEntity createUser(@RequestBody UserDTO userDTO) {
    	User user = new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getName(), userDTO.getLastname(), userDTO.getAddress());
    	userService.registerUser(user);
    	return  ResponseEntity.status(HttpStatus.OK).body(null);
    }
	
}
