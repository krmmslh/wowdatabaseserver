package app.repository;

import org.springframework.data.repository.CrudRepository;

import app.entity.Gear;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface GearRepository extends CrudRepository<Gear, Integer> {

}