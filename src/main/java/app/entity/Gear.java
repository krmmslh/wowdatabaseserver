package app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Gear {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String name;

    private String attr;

	public Integer getId() {
		return id;
	}

	public Gear() {
		
	}
	
	public Gear(String name, String attr) {
		super();
		this.name = name;
		this.attr = attr;
	}

	public Gear(Integer id, String name, String attr) {
		super();
		this.id = id;
		this.name = name;
		this.attr = attr;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}
	
	public String toString() {
		return " ID : " +id 	+ " NAME : " + name + " DESCRIPTION : " +attr;
	}

}
