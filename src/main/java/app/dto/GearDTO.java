package app.dto;

public class GearDTO {

	private int id;
	private String name;
	private String attr;
	public int getId() {
		return id;
	}
	
	public GearDTO() {
	}
	
	public GearDTO(int id, String name, String desc) {
		this.id = id;
		this.name = name;
		this.attr = desc;
	}
	
	
	public GearDTO(String name, String desc) {
		this.name = name;
		this.attr = desc;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String description) {
		this.attr = description;
	}
	
	public String toString() {
		return " name : " + name + " description " + attr;
	}
	
}
